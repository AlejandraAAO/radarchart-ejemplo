package com.example.radarchart;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Color;
import android.os.Bundle;

import com.github.mikephil.charting.charts.RadarChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.RadarData;
import com.github.mikephil.charting.data.RadarDataSet;
import com.github.mikephil.charting.data.RadarEntry;
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    //crea el objeto radarChart

    RadarChart radarChart;
    String[] labels = {"BMW","Volkswagen","Volvo","Audi","Lamborghini"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        radarChart = findViewById(R.id.radarChart);
        RadarDataSet dataSet1 = new RadarDataSet(dataValues1(),"Categoria 1");
        RadarDataSet dataSet2 = new RadarDataSet(dataValues2(),"Categoria 2");

        dataSet1.setColor(Color.rgb(237, 157, 126));
        dataSet1.setLineWidth(4);
        dataSet2.setColor(Color.rgb(133, 126, 237));
        dataSet2.setLineWidth(4);

        RadarData data = new RadarData();
        data.addDataSet(dataSet1);
        data.addDataSet(dataSet2);
        XAxis xAxis = radarChart.getXAxis();
        xAxis.setValueFormatter(new IndexAxisValueFormatter(labels));

        radarChart.setData(data);

        radarChart.invalidate();
    }

    private ArrayList<RadarEntry> dataValues1(){
        ArrayList<RadarEntry> dataVals = new ArrayList<>();
        dataVals.add(new RadarEntry(4));
        dataVals.add(new RadarEntry(7));
        dataVals.add(new RadarEntry(1));
        dataVals.add(new RadarEntry(5));
        dataVals.add(new RadarEntry(8));
        return dataVals;
    }

    private ArrayList<RadarEntry> dataValues2(){
        ArrayList<RadarEntry> dataVals = new ArrayList<>();
        dataVals.add(new RadarEntry(9));
        dataVals.add(new RadarEntry(2));
        dataVals.add(new RadarEntry(4));
        dataVals.add(new RadarEntry(1));
        dataVals.add(new RadarEntry(7));
        return dataVals;
    }


}
